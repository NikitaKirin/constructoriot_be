@push('head')
    <link
        href="{{asset('vendor/orchid/favicon-irit.ico')}}"
        id="favicon"
        rel="icon"
    >
@endpush

<p class="h2 n-m font-thin v-center">
    <img src="{{asset('vendor/orchid/favicon-irit.ico')}}" width="50"/>
    <span class="d-none d-sm-block" style="font-size: 25px">
        Навигатор ИОТ
        {{--<small class="v-top opacity">Nest</small>--}}
    </span>
</p>
